﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MusicPlayerCameraScript : MonoBehaviour {

	public AudioClip alarm;
	public AudioClip Scanning;
	public AudioSource audio;
	public Transform other;
	public float distance;
	private bool soundPlayed; 
	private int timeleft = 90;
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		AudioSource audio = GetComponent<AudioSource> ();

		distance = Vector3.Distance (other.position, transform.position); 

		if (distance <= 40 && !audio.isPlaying ) {
			audio.clip = alarm;
			audio.Play();
			float time = Time.deltaTime * 4;
			Soundbar.soundLevel += 4;

		} else if (distance <= 70 && !audio.isPlaying ) {
			audio.clip = Scanning;
			audio.Play ();

		} else {
			soundPlayed = false; 
		}
	}
}
