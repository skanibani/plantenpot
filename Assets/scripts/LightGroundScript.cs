﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGroundScript : MonoBehaviour {

	public float distance;
	public Transform other;
	public Light It;
	public float duration = 100.0F;
	public Color colorRed = Color.red;
	public Color colorGreen = Color.green;
	//spotlight
	public float interval = 0.3F;
	public float minAngle = 80; 
	public float maxAngle = 90;

 

	void Start () {
		It = GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {
		distance = Vector3.Distance (other.position, transform.position);
	
		if (distance <= 40  ) {
			It.color = Color.red; 
			// spot angle van 1 naar 179 gaat .
			It.spotAngle = Random.Range(minAngle, maxAngle);

		} else if (distance <= 70  ) {
	// switchen tussen rood en groen 
			float t = Mathf.PingPong (Time.time, duration) / duration;
			It.color = Color.Lerp (colorRed, colorGreen, t);
			// spot angle blijft default 
			It.spotAngle =  34.2f;
		
		} else {
			It.color = Color.green;
			// spot angle naar 179 en dan terug naar default  
		
				It.spotAngle =  179f;
		
			It.spotAngle = 34.2f;
				
		}
	}
}
