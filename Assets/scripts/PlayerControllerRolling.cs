﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerRolling : MonoBehaviour
{

    // Use this for initialization
    public float speed;

    private Rigidbody rb;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        float vx = Input.GetAxis("Horizontal");
        float vy = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(vx, 0.0f, vy);

        rb.AddForce(-movement * speed);
    }
}
