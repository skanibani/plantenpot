﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollision : MonoBehaviour {

	AudioSource audio;
    private ParticleSystem _psystem;


	void Start(){
		audio = GetComponent<AudioSource> ();
        _psystem = GetComponent<ParticleSystem>();

    }
	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.name == "player_prefab") {
			//StartCoroutine(gameObject.GetComponent<TriangleExplosion>().SplitMesh(true));
			audio.pitch = (Random.value * 12);
			audio.Play();
			Soundbar.soundLevel += 1;

            _psystem.Play();
			
        }	
	}
}

