﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {
	public int PlayerSpeed = 8;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.W)){
			transform.Translate(0, 0, PlayerSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.S)){
			transform.Translate(0, 0, -PlayerSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D)){
			transform.Translate(PlayerSpeed * Time.deltaTime,0, 0);
		}
		if(Input.GetKey(KeyCode.A)){
			transform.Translate(-PlayerSpeed * Time.deltaTime, 0, 0);
		}
	}
}
