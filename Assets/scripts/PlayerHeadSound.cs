﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeadSound : MonoBehaviour {

	public AudioClip[] sounds;
	public AudioClip scream_01;
	public AudioClip scream_02;
	public AudioClip scream_03;
	public AudioClip scream_04;

	public AudioSource audio;

	public int randomness;
	private int randomIndex;

	void Start(){
		sounds = new AudioClip[4];
		sounds [0] = scream_01 ;
		sounds [1] = scream_02 ;
		sounds [2] = scream_03 ;
		sounds [3] = scream_04 ;

		AudioSource audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider other){
		randomIndex = (int) Mathf.Ceil (Random.value * randomness);
		if (!audio.isPlaying && randomIndex <= 3) {
			audio.PlayOneShot (sounds[randomIndex], 0.3f);	
		}
}
}
