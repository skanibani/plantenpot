﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundbar : MonoBehaviour {

	public static int soundLevel;
	public int maxSoundLevel;

	void Start () {
		soundLevel = 0;
		maxSoundLevel = 0;
	}

	void ScaleSoundbar(){
		transform.localScale -= new Vector3 ((float) soundLevel / 10, 0, 0);
		maxSoundLevel += soundLevel;
		Soundbar.soundLevel = 0;
	}

	// Update is called once per frame
	void Update () {
		ScaleSoundbar ();
		if (maxSoundLevel == 100) {
			Application.LoadLevel("Game_Over");
		}
	}
}
